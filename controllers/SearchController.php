<?php

namespace app\controllers;

use app\models\Product;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;

class SearchController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'index' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex() {
        $post = Yii::$app->request->post();
        $products = [];

        if(isset($post['text'])) {
            $records = Product::find()->where(['like', 'name', $post['text']])->limit(10)->all();

            foreach ($records as $record) {
                $products[] = [
                    'id' => $record->id,
                    'name' => $record->name,
                    'main_thumb' => $record->main_thumb,
                    'price' => $record->price,
                ];
            }
        }

        return json_encode([
            'products' => $products,
        ]);
    }
}
