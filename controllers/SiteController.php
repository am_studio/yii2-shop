<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Login;
use app\models\Signup;
use app\models\ContactForm;
use app\models\Cart;

class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'add-in-cart' => ['post'],
                    'set-count' => ['post'],
                    'delete-from-cart' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        // = Yii::$app->request->post();
        //return Yii::$app->cart->getStatus();
        return $this->render('index');
    }

    /**
     * Login
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->getUser()->isGuest) {
            return $this->goHome();
        }

        $model = new Login();
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Signup new user
     * @return string
     */
    public function actionSignup()
    {
        $model = new Signup();
        if ($model->load(Yii::$app->getRequest()->post())) {
            if ($user = $model->signup()) {
                return $this->goHome();
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionAddInCart() {
        $post = Yii::$app->request->post();
        $cart = new Cart();

        $cart->add($post['product_id'], $post['count']);
        $products = [];

        if(!$cart->isEmpty()) {
            foreach ($cart->order->products as $product) {
                $products[] = [
                    'id' => $product->id,
                    'name' => $product->name,
                    'main_thumb' => $product->main_thumb,
                    'number' => $cart->order->countProduct($product->id),
                    'price' => $product->price,
                ];
            }
        }

        return json_encode([
            'status' => $cart->cart,
            'products' => $products,
        ]);
    }

    public function actionSetCount() {
        $post = Yii::$app->request->post();
        $cart = new Cart();

        $cart->setCount($post['product_id'], $post['count']);
        $products = [];

        if(!$cart->isEmpty()) {
            foreach ($cart->order->products as $product) {
                $products[] = [
                    'id' => $product->id,
                    'name' => $product->name,
                    'main_thumb' => $product->main_thumb,
                    'number' => $cart->order->countProduct($product->id),
                    'price' => $product->price,
                ];
            }
        }

        return json_encode([
            'status' => $cart->cart,
            'products' => $products,
        ]);
    }

    public function actionDeleteFromCart() {
        $post = Yii::$app->request->post();
        $cart = new Cart();

        $cart->delete($post['product_id']);
        $products = [];

        if(!$cart->isEmpty()) {
            foreach ($cart->order->products as $product) {
                $products[] = [
                    'id' => $product->id,
                    'name' => $product->name,
                    'main_thumb' => $product->main_thumb,
                    'number' => $cart->order->countProduct($product->id),
                    'price' => $product->price,
                ];
            }
        }

        return json_encode([
            'status' => $cart->cart,
            'products' => $products,
        ]);
    }
}
