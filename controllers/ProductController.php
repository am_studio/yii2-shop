<?php

namespace app\controllers;

use Yii;
use app\models\Category;
use app\models\Comment;
use app\models\Product;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\Pagination;

class ProductController extends Controller {
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'get-comments' => ['post'],
                ],
            ],
        ];
    }
    // Show all category
    public function actionIndex() {
        $model = Category::find()->where(['owner_id' => '-1'])->all();
        return $this->render('index', ['category' => $model]);
    }

    // Show category by id
    public function actionCategory($id) {
        $model = Category::findOne($id);

        if($model == null)
            throw new NotFoundHttpException('The requested page does not exist.');

        return $this->render('category', [
            'model' => $model, 'products' => $model->products
        ]);
    }

    public function actionGetComments() {
        $post = Yii::$app->request->post();
        $reviews = Comment::find()->where(['product_id' => $post['product_id']])
            ->offset($post['offset'])
            ->limit(5)->all();

        $comments = [];

        foreach ($reviews as $review) {
            $comments[] = [
                'id' => $review->id,
                'username' => $review->user->username,
                'text' => $review->text,
                'date_added' => $review->date_added,
                'rating' => $review->rating,
            ];
        }

        return json_encode([
            'product_id' => $post['product_id'],
            'count' => count($comments),
            'offset' => $post['offset'],
            'comments' => $comments,
        ]);
    }

    // Show product by id
    public function actionProduct($id) {
        $model = Product::findOne($id);

        if($model == null)
            throw new NotFoundHttpException('The requested page does not exist.');

        $comment = new Comment();
        $request = Yii::$app->request;

        if ($request->isPost) {
            if (Yii::$app->getUser()->isGuest) {
                $this->redirect('/site/login');
            } else {
                if ($comment->load($request->post())) {
                    $comment->user_id = \Yii::$app->user->identity->getId();
                    $comment->product_id = $id;

                    $date = new \DateTime();
                    $comment->date_added = $date->format('Y-m-d H:i:s');

                    if ($comment->save()) {
                        // Fix post
                        return $this->redirect(['product/product', 'id' => $model->id]);
                    }
                }
            }
        }

        $reviews = Comment::find()->where(['product_id' => $model->id])->limit(5)->all();

        /*$countQuery = clone $query;
        $pages = new Pagination([
            'totalCount' => $countQuery->count(),
            'pageSize' => 5]);

        $models = $query->offset($pages->offset)
            ->limit($pages->limit)
            ->all();*/

        return $this->render('product', [
            'model' => $model,
            'reviews' => $reviews,
            'comment' => $comment,
        ]);
    }
}
