<?php

namespace app\models;

use Yii;
use yii\imagine\Image;
use Imagine\Image\Box;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $price
 * @property string $main_thumb
 * @property integer $main_category
 *
 * @property CategoryToProduct[] $categoryToProducts
 * @property Category[] $categories
 * @property ProductToOrder[] $productToOrders
 * @property Order[] $orders
 * @property ThumbToProduct[] $thumbToProducts
 * @property Thumb[] $thumbs
 */
class Product extends \yii\db\ActiveRecord
{
    public $_thumbs = [];
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'main_category'], 'required'],
            [['description'], 'string'],
            [['price', 'special'], 'number'],
            [['main_category'], 'integer'],
            [['name'], 'string', 'max' => 256],

            [['main_thumb'], 'file', 'extensions' => 'png, jpg, jpeg'],
            ['main_thumb', 'required', 'on' => 'create'],

            [['_thumbs'], 'file', 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 4],
            [['_thumbs'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'special' => 'Special Price',
            'main_thumb' => 'Main Thumb',
            '_thumbs' => 'Thumbs',
            'main_category' => 'Category',
        ];
    }

    public function getCategory()
    {
        return $this->hasOne(Category::className(), [ 'id' => 'main_category'] );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToOrders()
    {
        return $this->hasMany(ProductToOrder::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('product_to_order', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumbToProducts()
    {
        return $this->hasMany(ThumbToProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumbs()
    {
        return $this->hasMany(Thumb::className(), ['id' => 'thumb_id'])->viaTable('thumb_to_product', ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getComments()
    {
        return $this->hasMany(Comment::className(), ['product_id' => 'id']);
    }

    // Upload photo
    public function upload($photo, $id, $resize = false) {
        $dir = 'uploads/products/';
        $name = 'product_id_'. $id . '_main.' . $photo->extension;
        $photo->saveAs($dir . $name);

        if($resize) {
            $thumbnail = 'product_id_'. $id . '_main_500x500.' . $photo->extension;
            Image::thumbnail('@webroot/' . $dir . $name, 500, 500)
                ->resize(new Box(500, 500))
                ->save(Yii::getAlias('@webroot/' . $dir . $thumbnail),
                    ['quality' => 70]);
            unlink($dir . $name);
        }
        return $resize ? ('/' . $dir . $thumbnail) : ('/' . $dir .$name);
    }

    // Upload thumbs
    public function uploadThumbs($thumbs, $id, $resize = false) {
        $count = 0;
        $thumbItems = [];

        $dirThumbs = 'uploads/products/thumbs/';

        foreach ($thumbs as $thumb) {
            $thumbItem = [];

            $name_pre = 'product_id_'. $id . '_fullsize_' . $count . '.' . $thumb->extension;
            $thumb->saveAs($dirThumbs . $name_pre);

            if($resize) {
                $url = 'product_id_'. $id . '_resize_' . $count . '.' . $thumb->extension;

                Image::thumbnail('@webroot/' . $dirThumbs . $name_pre, 500, 500)
                    ->resize(new Box(500, 500))
                    ->save(Yii::getAlias('@webroot/' . $dirThumbs . $url),
                        ['quality' => 70]);

                $dirPopup = 'uploads/products/thumbs/popup/';
                $popup = 'product_id_'. $id . '_resize_' . $count . '.' . $thumb->extension;
                Image::thumbnail('@webroot/' . $dirThumbs . $name_pre, 200, 200)
                    ->resize(new Box(200, 200))
                    ->save(Yii::getAlias('@webroot/' . $dirPopup . $popup),
                        ['quality' => 70]);

                unlink($dirThumbs .  $name_pre);

                $thumbItem['url'] = '/'.$dirThumbs.$url;
                $thumbItem['popup'] = '/'. $dirPopup.$popup;
            } else {
                $thumbItem['url'] = '/'.$dirThumbs.$name_pre;
                $thumbItem['popup'] = $thumbItem['url'];
            }

            $thumbItems[] = $thumbItem;
            $count++;
        }
        return $thumbItems;
    }

    // Delete photo if exists
    public function beforeDelete() {
        // Тут нужно доработать :)
        // substr($str, 1)
        $filename = $this->main_thumb;
        if(file_exists($filename)) {
            unlink($filename);
        }
        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}
