<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "thumb".
 *
 * @property integer $id
 * @property string $popup
 * @property string $url
 *
 * @property ThumbToProduct[] $thumbToProducts
 * @property Product[] $products
 */
class Thumb extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thumb';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['popup', 'url'], 'required'],
            [['popup'], 'string', 'max' => 256],
            [['url'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'popup' => 'Popup',
            'url' => 'Url',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumbToProducts()
    {
        return $this->hasMany(ThumbToProduct::className(), ['thumb_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('thumb_to_product', ['thumb_id' => 'id']);
    }
}
