<?php

namespace app\models;

use Yii;
use yii\base\Model;

class Cart extends Model {
    const SESSION_KEY = 'order_id';
    private $orderId;

    public function createOrder() {
        $order = new Order();
        if ($order->save()) {
            $this->orderId = $order->id;
            return true;
        }
        return false;
    }

    public function add($productId, $count) {
        $link = ProductToOrder::findOne(['product_id' => $productId, 'order_id' => $this->order->id]);
        if (!$link) {
            $link = new ProductToOrder();
        }
        $link->product_id = $productId;
        $link->order_id = $this->order->id;
        $link->count += $count;

        $product = Product::findOne(['id' => $productId]);
        $link->price = ($product->price * $count);
        return $link->save();
    }

    public function getOrder() {
        $order = Order::findOne(['id' => $this->getOrderId()]);
        if(!isset($order)) {
            $order = Order::findOne(['id' => $this->getOrderId( true)]);
        }
        return $order;
    }

    private function getOrderId($recycle = false) {
        if (!Yii::$app->session->has(self::SESSION_KEY) || $recycle) {
            if ($this->createOrder()) {
                Yii::$app->session->set(self::SESSION_KEY, $this->orderId);
            }
        }
        return Yii::$app->session->get(self::SESSION_KEY);
    }

    public function delete($productId) {
        $link = ProductToOrder::findOne(['product_id' => $productId, 'order_id' => $this->getOrderId()]);
        if (!$link) {
            return false;
        }
        // Clear empty cart
        if($this->isLastItem()) {
            $res = $link->delete();
            $this->finish(true);
            return $res;
        } else {
            return $link->delete();
        }
    }

    public function setCount($productId, $count) {
        $link = ProductToOrder::findOne(['product_id' => $productId, 'order_id' => $this->getOrderId()]);
        if (!$link) {
            return false;
        }
        $link->count = $count;
        $product = Product::findOne(['id' => $productId]);
        $link->price = ($product->price * $count);
        return $link->save();
    }

    public function getCart() {
        if ($this->isEmpty()) {
            return '0 item(s) - 0.00 UAH.';
        }
        return $this->order->count . ' item(s)' . ' - ' . $this->order->amount . ' UAH';
    }

    public function isEmpty() {
        if (!Yii::$app->session->has(self::SESSION_KEY)) {
            return true;
        }
        return $this->order->count ? false : true;
    }

    public function isLastItem() {
        if (!Yii::$app->session->has(self::SESSION_KEY)) {
            return true;
        }
        return $this->order->productsCount <= 1;
    }

    public function finish($clearDB = false) {
        if(Yii::$app->session->has(self::SESSION_KEY)) {
            if($clearDB) {
                Order::deleteAll(['id' => Yii::$app->session->get(self::SESSION_KEY)]);
            }
            $session = Yii::$app->session;

            // Delete session
            $session->remove(self::SESSION_KEY);
            unset($session[self::SESSION_KEY]);
            //unset($_SESSION[self::SESSION_KEY]);
        }
    }
}