<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property integer $address
 * @property integer $status
 *
 * @property ProductToOrder[] $productToOrders
 * @property Product[] $products
 */
class Order extends \yii\db\ActiveRecord
{
    const ORDER_IN_PROGRESS = 0; // Buyer lost cart
    const ORDER_NEW_ORDER = 1; // Buyer send order
    const ORDER_FINISH = 2; // Buyer get product

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'phone', 'address', 'status'], 'required', 'on' => ['checkout', 'update', 'create']],
            [['status'], 'integer'],
            [['first_name', 'last_name', 'email', 'phone'], 'string', 'max' => 255],
            ['address', 'string', 'max' => 512],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'phone' => 'Phone',
            'address' => 'Address',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductToOrders() {
        return $this->hasMany(ProductToOrder::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts() {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_to_order', ['order_id' => 'id']);
    }

    /**
     * @return integer
     */
    public function getCount() {
        return ProductToOrder::find()->where(['order_id' => $this->id])->sum('count');
    }

    /**
     * @return integer
     */
    public function getProductsCount() {
        return ProductToOrder::find()->where(['order_id' => $this->id])->count();
    }

    /**
     * @return integer
     */
    public function countProduct($product_id) {
        return ProductToOrder::find()->where(['order_id' => $this->id, 'product_id' => $product_id])->one()->count;
    }

    /**
     * @return integer
     */
    public function getAmount() {
        return ProductToOrder::find()->where(['order_id' => $this->id])->sum('price');
    }
}
