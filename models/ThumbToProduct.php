<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "thumb_to_product".
 *
 * @property integer $product_id
 * @property integer $thumb_id
 *
 * @property Product $product
 * @property Thumb $thumb
 */
class ThumbToProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'thumb_to_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['product_id', 'thumb_id'], 'required'],
            [['product_id', 'thumb_id'], 'integer'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['thumb_id'], 'exist', 'skipOnError' => true, 'targetClass' => Thumb::className(), 'targetAttribute' => ['thumb_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'thumb_id' => 'Thumb ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getThumb()
    {
        return $this->hasOne(Thumb::className(), ['id' => 'thumb_id']);
    }
}
