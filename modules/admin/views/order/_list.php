<?php
use yii\helpers\Html;
use yii\helpers\HtmlPurifier;
?>

<div class="product-item">
    <div class="image">
        <img src=" <?= $product->main_thumb; ?>" title="<?= $product->name; ?>" alt="<?= $product->name; ?>">
    </div>
    <div class="name">
        <?= $product->name; ?>
        <div class="total">
            <span class="number"><?= $order->countProduct($product->id); ?></span> x
            <span class="price"><?= $product->price; ?></span> UAH.
        </div>
    </div>
</div>