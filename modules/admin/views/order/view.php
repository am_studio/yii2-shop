<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\widgets\ListView;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $model app\models\Order */

$this->title = 'Order id: '.$model->id;
$this->params['breadcrumbs'][] = ['label' => 'Orders', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;

$provider = new ArrayDataProvider([
    'allModels' => $model->products,
    'pagination' => [
        'pageSize' => 10,
    ],
    'sort' => [
        'attributes' => ['id', 'name'],
    ],
]);
?>
<div class="order-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <h3>Base info:</h3>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'first_name',
            'last_name',
            'email:email',
            'phone',
            'address',
            'status' => [
                'attribute' => 'status',
                'format' => 'text',
                'value' => function ($model) use ($states) {
                    return $states[$model->status];
                }
            ],
        ],
    ]) ?>

    <h3>Products:</h3>
    <?php Pjax::begin(); ?>
    <div class="product-group">
        <?= ListView::widget([
            'dataProvider' => $provider,
            'itemView' => function ($product, $key, $index, $widget) use($model) {
                return $this->render('_list',['product' => $product, 'order' => $model]);
            },
        ]); ?>
    </div>
    <?php Pjax::end(); ?>
</div>
