<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CommentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Comments';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="comment-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Comment', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'text',
            'product_id' => [
                'attribute' => 'product_id',
                'format' => 'html',
                'value' => function ($model) {
                    $product = $model->product;
                    return "<a href=\"" . Url::toRoute(['/admin/product/view/', 'id' => $product->id]) . "\">" . $product->name . "</a>";
                }
            ],
            'user_id' => [
                'attribute' => 'user_id',
                'value' => function ($model) {
                    return $model->user->username;
                }
            ],
            'rating',
            'date_added',
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
