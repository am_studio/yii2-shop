<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\\models\ProductSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Product', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            'main_thumb' => [
                'attribute' => 'main_thumb',
                'format' => 'html',
                'label' => 'Photo',
                'value' => function ($model) {
                    return Html::img($model->main_thumb, ['style' => 'max-width: 220px;']);
                }
            ],
            'name',
            'price',
            'main_category' => [
                'attribute' => 'main_category',
                'filter' => $category,
                'value' => function ($model) {
                    return $model->category->title;
                }
            ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
