<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Product */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'description:html',
            'price',
            'main_thumb' => [
                'attribute' => 'main_thumb',
                'format' => 'html',
                'label' => 'Photo',
                'value' => function ($model) {
                    return Html::img($model->main_thumb, ['style' => 'max-width: 220px;']);
                }
            ],
            'main_category' => [
                'attribute' => 'main_category',
                'format' => 'text',
                'value' => function ($model) {
                    return $model->category->title;
                }
            ],
        ],
    ]) ?>

</div>
