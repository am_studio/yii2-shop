<?php

namespace app\modules\admin\controllers;

use app\models\Thumb;
use app\models\ThumbToProduct;
use Yii;
use app\models\Product;
use app\models\Category;
use app\models\ProductSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

/**
 * ProductController implements the CRUD actions for Product model.
 */
class ProductController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Product models.
     * @return mixed
     */
    public function actionIndex() {
        $searchModel = new ProductSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $category = ArrayHelper::map(Category::find()->all(), 'id', 'title');

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'category' => $category
        ]);
    }

    /**
     * Displays a single Product model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id) {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Product model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate() {
        $model = new Product();
        $model->scenario = 'create';

        if ($model->load(Yii::$app->request->post())) {
            // Set default image
            $model->main_thumb = '/uploads/placeholder.png';

            if($model->save()) {
                // Add photos
                $photo = UploadedFile::getInstance($model, 'main_thumb');
                $name = $model->upload($photo, $model->id, true);
                $model->main_thumb = $name;

                //$thumb = new Thumb;
                $_thumbs = UploadedFile::getInstances($model, '_thumbs');
                $thumbItems = $model->uploadThumbs($_thumbs, $model->id, true);

                foreach ($thumbItems as $thumbItem) {
                    $thumb = new Thumb;
                    $thumb->popup = $thumbItem['popup'];
                    $thumb->url = $thumbItem['url'];
                    $thumb->save();

                    // Set connection
                    $connect = new ThumbToProduct;
                    $connect->product_id = $model->id;
                    $connect->thumb_id = $thumb->id;
                    $connect->save();
                }

                // Save changes
                $model->save();
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $category = ArrayHelper::map(Category::find()->all(), 'id', 'title');
        return $this->render('create', [
            'model' => $model, 'category' => $category
        ]);
    }

    /**
     * Updates an existing Product model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id) {
        $model = $this->findModel($id);
        $oldPhoto =  $model->main_thumb;

        if ($model->load(Yii::$app->request->post())) {
            $model->main_thumb = $oldPhoto;

            if($model->save()) {
                if($_FILES['Product']['name']['main_thumb'] != "" ||
                   $_FILES['Product']['name']['_thumbs'] != "") {
                    if($_FILES['Product']['name']['main_thumb'] != "") {
                        $photo = UploadedFile::getInstance($model, 'main_thumb');
                        $name = $model->upload($photo, $model->id, true);
                        $model->main_thumb = $name;
                    }
                    if($_FILES['Product']['name']['_thumbs'] != "") {
                        //$thumb = new Thumb;
                        $_thumbs = UploadedFile::getInstances($model, '_thumbs');
                        $thumbItems = $model->uploadThumbs($_thumbs, $model->id, true);

                        foreach ($thumbItems as $thumbItem) {
                            $thumb = new Thumb;
                            $thumb->popup = $thumbItem['popup'];
                            $thumb->url = $thumbItem['url'];
                            $thumb->save();

                            // Set connection
                            $connect = new ThumbToProduct;
                            $connect->product_id = $model->id;
                            $connect->thumb_id = $thumb->id;
                            $connect->save();
                        }
                    }
                    // Save changes
                    $model->save();
                }
                return $this->redirect(['view', 'id' => $model->id]);
            }
        }

        $category = ArrayHelper::map(Category::find()->all(), 'id', 'title');
        return $this->render('update', [
            'model' => $model, 'category' => $category,
        ]);
    }

    /**
     * Deletes an existing Product model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Product the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Product::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
