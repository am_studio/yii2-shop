<?php

namespace app\modules\order\controllers;

use Yii;
use yii\web\Controller;
use app\models\Cart;
use app\models\Order;

/**
 * Default controller for the `order` module
 */
class DefaultController extends Controller {
    /**
     * Renders the index view for the module
     * @return string
     */
    public function actionIndex() {
        $cart = new Cart();

        if($cart->isEmpty()) {
            return $this->render('empty');
        } else {
            $model = $cart->order;
            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                $model->scenario = 'checkout';
                $model->status = Order::ORDER_NEW_ORDER;
                if($model->save()) {
                    $cart->finish(); // Clear session (clear db flag)
                    return $this->redirect(['finish', 'id' => $model->id]);
                }
            }

            return $this->render('index', [
                'model' => $model, 'cart' => $cart,
            ]);
        }
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionFinish($id) {
        return $this->render('finish', [
            'model' => Order::find($id),
        ]);
    }
}
