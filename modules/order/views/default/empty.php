<?php
$this->title = 'Order';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-default-finish">
    <h1>Cart empty</h1>
    <p>Your cart is empty, add some products to it</p>
</div>
