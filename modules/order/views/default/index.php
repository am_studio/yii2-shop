<?php

use yii\widgets\ListView;
use yii\data\ArrayDataProvider;
use yii\widgets\Pjax;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Order';
$this->params['breadcrumbs'][] = $this->title;

$provider = new ArrayDataProvider([
    'allModels' => $cart->order->products,
    'pagination' => [
        'pageSize' => 5,
    ],
]);
?>

<div class="order-default-index">
    <h1>Order</h1>

    <div class="row">
        <div class="col-sm-6">
            <h3>Total:</h3>
            <h4><?= $cart->cart; ?></h4>
            <h3>Products:</h3>
            <?php Pjax::begin(); ?>
            <div class="product-group">
                <?= ListView::widget([
                    'dataProvider' => $provider,
                    'itemView' => function ($product, $key, $index, $widget) use($model) {
                        return $this->render('_list',['product' => $product, 'order' => $model]);
                    },
                ]); ?>
            </div>
            <?php Pjax::end(); ?>
        </div>
        <div class="col-sm-6">
            <h3>User data:</h3>
            <?php $form = ActiveForm::begin(); ?>

            <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'phone')->textInput(['maxlength' => true]) ?>

            <?= $form->field($model, 'address')->textInput() ?>

            <div class="form-group">
                <?= Html::submitButton('Checkout', ['class' => 'btn btn-primary']) ?>
            </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
