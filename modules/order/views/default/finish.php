<?php
$this->title = 'Order accepted';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="order-default-finish">
    <h1>Order accepted</h1>
    <p>Order accepted, wait for manager's call</p>
</div>
