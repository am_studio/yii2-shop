<?php

use yii\db\Migration;

/**
 * Class m171214_165258_comment
 */
class m171214_165258_comment extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->createTable('comment', [
			'id' => $this->primaryKey(),
			'product_id' => $this->integer(11)->notNull(),
			'user_id' => $this->integer(11)->notNull(),
			'rating' => $this->smallInteger(1)->defaultValue(0),
			'date_added' => $this->dateTime(),
			'text' => $this->string(512)->notNull(),
		]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('comment');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171214_165258_comment cannot be reverted.\n";

        return false;
    }
    */
}
