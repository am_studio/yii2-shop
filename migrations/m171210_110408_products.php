<?php

use yii\db\Migration;

/**
 * Class m171210_110408_products
 */
class m171210_110408_products extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
		$this->createTable('product', [
			'id' => $this->primaryKey(),
            'name' => $this->string(256)->notNull(),
            'description' => $this->text()->notNull(),
			'main_thumb' => $this->string(256)->notNull(),
			'main_category' => $this->integer()->notNull(),
            'price' => $this->decimal(10, 2),
            'special' => $this->decimal(10, 2),
		]);
		
		$this->createTable('category', [
			'id' => $this->primaryKey(),
			'title' => $this->string(255)->notNull(),
			'thumb' => $this->string(256)->notNull(),
		]);

		/* Category add Begin */
        $this->insert('category', [
            'title' => 'Телефони',
            'thumb' => '/uploads/category/phone.jpg',
        ]);

        $this->insert('category', [
            'title' => 'Телевізори',
            'thumb' => '/uploads/category/tv.jpg',
        ]);

        $this->insert('category', [
            'title' => 'Ноутбуки',
            'thumb' => '/uploads/category/notebook.jpg',
        ]);

        $this->insert('category', [
            'title' => 'Фото-апарати',
            'thumb' => '/uploads/category/photo.jpg',
        ]);

        $this->insert('category', [
            'title' => 'Пилососи',
            'thumb' => '/uploads/category/cleaners.jpg',
        ]);

        $this->insert('category', [
            'title' => 'Кавові машини',
            'thumb' => '/uploads/category/coffe.jpg',
        ]);
        /* Category add End */
		
		$this->createTable('thumb', [
			'id' => $this->primaryKey(),
			'popup' => $this->string(255)->notNull(),
			'url' => $this->string(255)->notNull(),
		]);
		
		$this->createTable('thumb_to_product', [
            'product_id' => $this->integer()->notNull(),
            'thumb_id' => $this->integer()->notNull()
        ]);
		
		$this->addForeignKey('thumb_to_product_product_fk', 'thumb_to_product', 'product_id', 'product', 'id', 'CASCADE');
		$this->addForeignKey('thumb_to_product_thumb_fk', 'thumb_to_product', 'thumb_id', 'thumb', 'id', 'CASCADE');
		$this->addPrimaryKey('thumb_to_product_pk', 'thumb_to_product', ['product_id', 'thumb_id']);
		
		//$this->addForeignKey('product_connection_fk', 'product', 'category_id', 'category', 'id', 'CASCADE');
		
		$this->createTable('order', [
			'id' => $this->primaryKey(),
			'first_name' => $this->string(255)->notNull(),
			'last_name' => $this->string(255)->notNull(),
			'email' => $this->string(255)->notNull(),
			'phone' => $this->string(255)->notNull(),
			'address' => $this->string(512)->notNull(),
			'status' => $this->smallInteger(1)->defaultValue(0),
		]);
		
		$this->createTable('product_to_order', [
			'product_id' => $this->integer(11)->notNull(),
			'order_id' => $this->integer(11)->notNull(),
			'count' => $this->integer(11)->defaultValue(1),
			'price' => $this->decimal(10, 2),
		]);
		
		$this->addForeignKey('product_to_order_product_fk', 'product_to_order', 'product_id', 'product', 'id', 'CASCADE');
		$this->addForeignKey('product_to_order_order_fk', 'product_to_order', 'order_id', 'order', 'id', 'CASCADE');
		$this->addPrimaryKey('product_to_order_pk', 'product_to_order', ['product_id', 'order_id']);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {		
		$this->dropForeignKey(
            'thumb_to_product_product_fk',
            'thumb_to_product'
        );
		
		$this->dropForeignKey(
            'thumb_to_product_thumb_fk',
            'thumb_to_product'
        );
		
		$this->dropForeignKey(
            'product_to_order_product_fk',
            'product_to_order'
        );
		
		$this->dropForeignKey(
            'product_to_order_order_fk',
            'product_to_order'
        );
		
		$this->dropTable('product');
		$this->dropTable('category');
		$this->dropTable('thumb');
		$this->dropTable('order');
		
		$this->dropTable('thumb_to_product');
		$this->dropTable('product_to_order');
        /**echo "m171210_110408_products cannot be reverted.\n";

        return false;*/
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171210_110408_products cannot be reverted.\n";

        return false;
    }
    */
}
