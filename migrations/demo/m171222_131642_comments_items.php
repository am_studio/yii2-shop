<?php

use yii\db\Migration;

/**
 * Class m171222_131642_comments_items
 */
class m171222_131642_comments_items extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp() {
		
		for($i = 0; $i < 10; $i++) {
			$this->insert('comment', [
				'product_id' => 8,
				'user_id' => 1,
				'rating' => rand(0,5),
				'date_added' => '2017-12-21 10:29:1'. $i,
				'text' => 'Коментар, до 256 символів, номер: ' . $i,
			]);
		}
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
		$this->delete('comment');
        /*echo "m171222_131642_comments_items cannot be reverted.\n";
        return false;*/
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171222_131642_comments_items cannot be reverted.\n";

        return false;
    }
    */
}
