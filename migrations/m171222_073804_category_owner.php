<?php

use yii\db\Migration;

/**
 * Class m171222_073804_category_owner
 */
class m171222_073804_category_owner extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp() {
		// ALTER TABLE `category` ADD `owner_id` INT(11) NOT NULL AFTER `title`;
		$this->addColumn('category', 'owner_id', 'integer NOT NULL default -1');
		$this->addColumn('category', 'description', 'text NOT NULL');
		
		/* Category add Begin */
        $this->insert('category', [
            'title' => 'Смартфони',
            'thumb' => '/uploads/category/phone_smart.jpg',
			'owner_id' => 1,
			'description' => 'Смартфон (англ. smartphone умный телефон) - мобильный телефон, дополненный функциональностью карманного персонального компьютера.
Также коммуникатор (англ. communicator, PDA phone) - карманный персональный компьютер, дополненный функциональностью мобильного телефона.',
        ]);
		
		 $this->insert('category', [
            'title' => 'Бюджетні',
            'thumb' => '/uploads/category/phone_price.jpg',
			'owner_id' => 1,
			'description' => 'Смартфон (англ. smartphone умный телефон) - мобильный телефон, дополненный функциональностью карманного персонального компьютера.
Также коммуникатор (англ. communicator, PDA phone) - карманный персональный компьютер, дополненный функциональностью мобильного телефона.',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown() {
        /*echo "m171222_073804_category_owner cannot be reverted.\n";
        return false;*/
		$this->delete('category', ['owner_id' => 1]);
		$this->dropColumn('category', 'owner_id');
		$this->dropColumn('category', 'description');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m171222_073804_category_owner cannot be reverted.\n";

        return false;
    }
    */
}
