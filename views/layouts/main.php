<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use app\widgets\CartWidget;
use app\widgets\LoginWidget;
use app\widgets\SearchWidget;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use app\models\Cart;

use app\widgets\MultiLanguageWidget;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<?php
NavBar::begin([
    'brandLabel' => Yii::$app->name,
    'brandUrl' => Yii::$app->homeUrl,
    /*'options' => [
        'class' => 'navbar-inverse navbar-fixed-top',
    ],*/
]);
echo Nav::widget([
    'options' => ['class' => 'navbar-nav navbar-right'],
    'items' => [
        ['label' => 'Home', 'url' => ['/site/index']],
        ['label' => 'Products', 'url' => ['/product/index']],
        ['label' => 'About', 'url' => ['/site/about']],
        ['label' => 'Contact', 'url' => ['/site/contact']],
        LoginWidget::widget(),
        MultiLanguageWidget::widget([
            'width'       => '18',
            'calling_controller' => $this->context
        ]),
    ],
]);
NavBar::end();
?>
<header>
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
               <?php echo SearchWidget::widget(); ?>
            </div>
            <div class="col-sm-3">
                <?php echo CartWidget::widget([
                    'model' => new Cart()
                ]); ?>
            </div>
        </div>
    </div>
</header>

<main>
<div class="container">
    <?= Breadcrumbs::widget([
        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
    ]) ?>
    <?= Alert::widget() ?>
    <?= $content ?>
</div>
</main>

<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <h5>Header 1</h5>
                <ul class="list-unstyled">
                    <li><a href="#">Footer link 1</a></li>
                    <li><a href="#">Footer link 2</a></li>
                    <li><a href="#">Footer link 3</a></li>
                    <li><a href="#">Footer link 4</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5>Header 2</h5>
                <ul class="list-unstyled">
                    <li><a href="#">Footer link 1</a></li>
                    <li><a href="#">Footer link 2</a></li>
                    <li><a href="#">Footer link 3</a></li>
                    <li><a href="#">Footer link 4</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5>Header 3 </h5>
                <ul class="list-unstyled">
                    <li><a href="#">Footer link 1</a></li>
                    <li><a href="#">Footer link 2</a></li>
                    <li><a href="#">Footer link 3</a></li>
                    <li><a href="#">Footer link 4</a></li>
                </ul>
            </div>
            <div class="col-sm-3">
                <h5>Header 4</h5>
                <ul class="list-unstyled">
                    <li><a href="#">Footer link 1</a></li>
                    <li><a href="#">Footer link 2</a></li>
                    <li><a href="#">Footer link 3</a></li>
                    <li><a href="#">Footer link 4</a></li>
                </ul>
            </div>
        </div>
        <hr>
        <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<script>
    var language = '<?= Yii::$app->language ?>';
    var languageSource = '<?= Yii::$app->sourceLanguage ?>';
</script>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
