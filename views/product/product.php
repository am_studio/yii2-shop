<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use yii\widgets\LinkPager;
use kartik\rating\StarRating;
use yii\widgets\Pjax;

$this->title = $model->name;

$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['/product/']];
$this->params['breadcrumbs'][] = ['label' => $model->category->title,
    'url' => [
        Url::toRoute([
            '/product/category/',
            'language' => '',
            'id' => $model->main_category
        ])
    ]
];
$this->params['breadcrumbs'][] = $this->title;
?>

<h1><?php echo $model->name; ?></h1>
<hr>

<div id="content">
    <div class="row">
        <div class="col-sm-8">

            <?php if ($model->main_thumb || $model->thumbs) { ?>
                <ul class="thumbnails">
                    <?php if ($model->main_thumb) { ?>
                        <li><a class="thumbnail" href="<?php echo $model->main_thumb; ?>" title="<?php echo $model->name; ?>"><img src="<?php echo $model->main_thumb; ?>" title="<?php echo $model->name; ?>" alt="<?php echo $model->name; ?>" /></a></li>
                    <?php } ?>
                    <?php if ($model->thumbs) { ?>
                        <?php foreach ($model->thumbs as $thumb) { ?>
                            <li class="image-additional"><a class="thumbnail" href="<?php echo $thumb->url; ?>" title="<?php echo $model->name; ?>"> <img src="<?php echo $thumb->popup; ?>" title="<?php echo $model->name; ?>" alt="<?php echo $model->name; ?>" /></a></li>
                        <?php } ?>
                    <?php } ?>
                </ul>
            <?php } ?>

            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab-description" data-toggle="tab">Description</a></li>
                <li><a href="#tab-review" data-toggle="tab">Comments</a></li>
            </ul>

            <div class="tab-content">
                <div class="tab-pane active" id="tab-description">
                    <?= $model->description; ?>
                </div>
                 <div class="tab-pane" id="tab-review">
                     <?php if (count($reviews)) { ?>
                         <h2>Comments</h2>
                         <div id="reviews">
                             <div class="review-items">
                             <?php foreach ($reviews as $review) { ?>
                                 <div class="review-item">
                                     <div class="review-header">
                                         <div class="pull-left">
                                             <strong><?php echo $review->user->username; ?></strong>
                                         </div>
                                         <div class="pull-right">
                                             <?php echo $review->date_added; ?>
                                         </div>
                                     </div>
                                     <div class="review-content">
                                         <p><?php echo $review->text; ?></p>
                                     </div>
                                     <div class="review-footer">
                                         <span class="raty" data-score="<?= $review->rating ?>"></span>
                                     </div>
                                 </div>
                             <?php } ?>
                             </div>

                             <div class="loadder">
                                <a href="#" onclick="loadMore(<?= $model->id ?>, this); return false;" class="btn btn-success btn-lg"><i class="glyphicon glyphicon-refresh"></i> Load more...</a>
                             </div>

                             <script>
                                 $(document).ready(function() {
                                     $('.raty').raty({
                                         readOnly: true,
                                         starType : 'i',
                                         starOff : 'fa fa-star-o',
                                         starOn  : 'fa fa-star',
                                     });
                                 });
                             </script>
                         </div>
                         <hr>
                     <?php } ?>
                     <?= $this->render('_comment', [
                         'model' => $comment,
                     ]) ?>
                 </div>
            </div>

        </div>
        <div class="col-sm-4">
            <div id="product">
                <ul class="list-unstyled">
                    <li>Category: <span><?php echo $model->category->title; ?></span></li>
                    <li>Price: <span><?php echo $model->price; ?></span></li>
                </ul>
                <div class="form-group">
                    <label class="control-label" for="input-quantity">Count</label>
                    <input type="number" name="quantity" value="1" min="1" id="input-quantity" class="form-control" />
                    <input type="hidden" name="product_id" value="<?php echo $model->id; ?>" />
                    <br />
                    <button type="button" id="button-cart" onclick="cartAdd(<?php echo $model->id; ?>, $('#input-quantity').val()); return false;" class="btn btn-primary btn-lg btn-block">Add to cart</button>
                </div>
                <a href="" onclick="$('a[href=\'#tab-review\']').trigger('click'); return false;">Comments: <?php echo count($reviews); ?></a>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('.thumbnails').magnificPopup({
            type:'image',
            delegate: 'a',
            gallery: {
                enabled:true
            }
        });
    });
</script>