<?php

/* @var $this yii\web\View */
use yii\helpers\Url;

$this->title = 'Products';
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
    <div class="row">
        <?php if(count($category)) { ?>
            <?php foreach ($category as $cat) { ?>
                <div class="product-layout col-sm-6 col-md-3">
                    <div class="product-thumb">
                        <div class="image">
                            <a href="<?php echo Url::toRoute(['product/category', 'id' => $cat->id]); ?>">
                                <img src="<?php echo $cat->thumb; ?>" alt="<?php echo $product->title; ?>" title="<?php echo $cat->title; ?>" class="img-responsive category-img"/>
                            </a>
                        </div>
                        <h3 class="text-center">
                            <a href="<?php echo Url::toRoute(['product/category', 'id' => $cat->id]); ?>"><?php echo $cat->title; ?></a>
                        </h3>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
    </div>
</div>