<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
use app\widgets\CategoryWidget;

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Products', 'url' => ['/product/']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div id="content">
    <div class="row">
        <div class="col-sm-3">
            <?php echo CategoryWidget::widget(['id' => $model->id, 'owner_id' => $model->owner_id]); ?>
        </div>
        <div class="col-sm-9 category">
            <h1><?= $model->title; ?></h1>
            <?php if($model->description) { ?>
            <div class="row">
                <div class="col-sm-12">
                    <div class="description">
                        <div class="image thumbnail">
                            <img src="<?= $model->thumb; ?>" alt="<?= $model->title; ?>" title="<?= $model->title; ?>" class="img-responsive"/>
                        </div>
                        <div class="text">
                            <?= $model->description ?>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <?php } ?>
            <div class="row">
                <?php if(count($products)) { ?>
                    <?php foreach ($products as $product) { ?>
                        <div class="product-layout col-sm-6 col-md-4">
                            <div class="product-thumb">
                                <div class="image"><a href="<?php echo Url::toRoute(['product/product', 'id' => $product->id]); ?>"><img src="<?php echo $product->main_thumb; ?>" alt="<?php echo $product->name; ?>" title="<?php echo $product->name; ?>" class="img-responsive" /></a></div>
                                <div>
                                    <div class="caption">
                                        <h4><a href="<?php echo Url::toRoute(['product/product', 'id' => $product->id]); ?>"><?php echo $product->name; ?></a></h4>
                                        <p><?php echo mb_substr(strip_tags($product->description), 0, 128).'...'; ?></p>
                                        <?php if (isset($product->rating) && $product->rating) { ?>
                                            <div class="rating">
                                                <?php echo StarRating::widget([
                                                    'name' => 'rating',
                                                    'value' => $product->rating,
                                                    'pluginOptions' => [
                                                        'min' => 0,
                                                        'max' => 5,
                                                        'step' => 1,
                                                        'size' => 'xs',
                                                        'showClear' => false,
                                                        'showCaption' => false,
                                                        'displayOnly' => true
                                                    ]
                                                ]); ?>
                                            </div>
                                        <?php } ?>
                                    </div>
                                    <div class="button-group">
                                        <button type="button" onclick="cartAdd(<?php echo $product->id; ?>, 1); return false;">
                                            <i class="fa fa-shopping-cart"></i> <span class="hidden-xs hidden-sm hidden-md">Add to cart</span>
                                        </button>
                                    </div>
                                </div>
                                <?php if ($product->price) { ?>
                                    <p class="price">
                                        <?php echo $product->price; ?> UAH
                                    </p>
                                <?php } ?>
                            </div>
                        </div>
                    <?php } ?>
                <?php } ?>
            </div>
        </div>
    </div>
</div>