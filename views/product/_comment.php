<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\widgets\StarRating;

/* @var $this yii\web\View */
/* @var $model app\models\Product */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="comment-form">
    <h2>Add comment</h2>

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'text')->textarea(['rows' => 5]) ?>

    <?= $form->field($model, 'rating')->widget(StarRating::classname(), ['rating' => 0]); ?>

    <div class="form-group">
        <?= Html::submitButton('Send comment', ['class' => 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>