function loadMore(id, obj) {
    var local = languageSource.split('-')[0] == language ? '' : '/' + language;

    $.ajax({
        url: local + "/product/get-comments",
        method: 'post',
        data: {
            'product_id' : id,
            'offset' : $('#reviews .review-items .review-item').length,
            /*'_csrf' : _csrf*/
        },
        beforeSend: function() {
            $(obj).find('i').addClass('gly-spin');
        },
        success: function(data) {
            setTimeout(function(){
                $(obj).find('i').removeClass('gly-spin');

                var result = JSON.parse(data);

                if(result.comments.length > 0) {
                    var parrent = $('#reviews .review-items');

                    result.comments.forEach(function (item, i, arr) {
                        /*console.log(item);*/
                        var html = "<div class=\"review-item\">\n" +
                            "                                     <div class=\"review-header\">\n" +
                            "                                         <div class=\"pull-left\">\n" +
                            "                                             <strong>" + item.username + "</strong>\n" +
                            "                                         </div>\n" +
                            "                                         <div class=\"pull-right\">\n" +
                            "                                             " + item.date_added + "\n" +
                            "                                         </div>\n" +
                            "                                     </div>\n" +
                            "                                     <div class=\"review-content\">\n" +
                            "                                         <p>" + item.text + "</p>\n" +
                            "                                     </div>\n" +
                            "                                     <div class=\"review-footer\">\n" +
                            "                                         <span class=\"raty\" data-score=\"" + item.rating + "\"></span>\n" +
                            "                                     </div>\n" +
                            "                                 </div>";

                        $(html).hide().appendTo(parrent).fadeIn(300);
                    });
                    //parrent.append(html).fadeIn();

                    $('.raty').raty({
                        readOnly: true,
                        starType : 'i',
                        starOff : 'fa fa-star-o',
                        starOn  : 'fa fa-star',
                    });

                    if(result.count < 5) {
                        $('#reviews .loadder').hide();
                    }
                } else {
                    $('#reviews .loadder').hide();
                }
            }, 500);
        }
    }).done(function(data) {
        console.log(data);
    });
}

function cartMinus(id, obj) {
    var button = $(obj);
    var parent = button.parent();
    var input = parent.find('input');
    var count = parseInt(input.val()) - 1;

    if(count == 0) {
        count++;
    }
    cartCount(id, count);
}

function cartPlus(id, obj) {
    var button = $(obj);
    var parent = button.parent();
    var input = parent.find('input');
    var count = parseInt(input.val()) + 1;

    if(count > 100) {
        count = 100;
    }
    cartCount(id, count);
}

function cartCount(id, count, _csrf) {
    var local = languageSource.split('-')[0] == language ? '' : '/' + language;

    $.ajax({
        url: local + "/site/set-count",
        method: 'post',
        data: {
            'product_id' : id,
            'count' : count,
            /*'_csrf' : _csrf*/
        },
        beforeSend: function() {

        },
        success: function(data) {
            //console.log(data);
            var result = JSON.parse(data);
            $('#cart-total').html(result.status);
            cartRecycle(result.products);
            /*alert(result.success);*/
        }
    }).done(function(data) {
        /*console.log(data);*/
        /*$('html, body').animate({
            scrollTop: $("#cart").offset().top - 15
        }, 500);*/
    });
}

function cartAdd(id, count, _csrf) {
    var local = languageSource.split('-')[0] == language ? '' : '/' + language;

    $.ajax({
        url: local + "/site/add-in-cart",
        method: 'post',
        data: {
            'product_id' : id,
            'count' : count,
            /*'_csrf' : _csrf*/
        },
        beforeSend: function() {

        },
        success: function(data) {
            /*console.log(data);*/
            var result = JSON.parse(data);
            $('#cart-total').html(result.status);
            cartRecycle(result.products);
            /*alert(result.success);*/
        }
    }).done(function(data) {
        /*console.log(data);*/
        $('html, body').animate({
            scrollTop: $("#cart").offset().top - 15
        }, 500);
    });
}

function cartRemove(id, _csrf) {
    var local = languageSource.split('-')[0] == language ? '' : '/' + language;

    $.ajax({
        url: local + "/site/delete-from-cart",
        method: 'post',
        data: {
            'product_id' : id,
            /*'_csrf' : _csrf*/
        },
        beforeSend: function() {

        },
        success: function(data) {
            /*console.log(data);*/
            var result = JSON.parse(data);
            $('#cart-total').html(result.status);

            cartRecycle(result.products);
        }
    }).done(function(data) {
        /*console.log(data);*/
    });
}

function cartRecycle(products) {
    var local = languageSource.split('-')[0] == language ? '' : '/' + language;

    var parrent = $('#cart li');
    if(products.length > 0) {
        var html = "<div class=\"list-group\">";
        products.forEach(function (item, i, arr) {
            /*console.log(item);*/
            html += "<div class=\"list-group-item\">\n" +
                "                        <div class=\"image\">\n" +
                "                            <img src=\"" + item.main_thumb + "\" title=\"" + item.name + "\" alt=\"" + item.name + "\" />\n" +
                "                        </div>\n" +
                "                        <div class=\"name\">\n" +
                "                            <span class=\"truncate-line\"><a href=\"" + local + "/product/product?id=" + item.id + "\" class=\"link\">" + item.name + "</a></span>\n" +
                "                        </div>\n" +
                "                        <div class=\"total hidden-xs\">\n" +
                "                            <div class=\"number-group\">\n" +
                "                                <div class=\"input-group\">\n" +
                "                                    <span class=\"input-group-addon\" onclick=\"cartMinus(" + item.id + ", this)\">-</span>\n" +
                "                                    <input class=\"form-control\" value=\"" + item.number + "\" readonly>\n" +
                "                                    <span class=\"input-group-addon\" onclick=\"cartPlus(" + item.id + ", this)\">+</span>\n" +
                "                                </div>\n" +
                "                            </div>\n" +
                "                        </div>\n" +
                "                        <div class=\"price\">\n" +
                "                            " + item.price + " UAH.\n" +
                "                        </div>\n" +
                "                        <div class=\"buttons\">\n" +
                "                            <button type=\"button\" class=\"btn btn-danger\" onclick=\"cartRemove(" + item.id + ");\" >x</button>\n" +
                "                        </div>\n" +
                "                    </div>";
        });
        html += "<div class=\"list-group-item\">\n" +
            "                        <a href=\"" + local + "/order\" class=\"btn btn-success btn-block\"><strong><i class=\"fa fa-share\"></i> Check Out</strong></a>\n" +
            "                    </div>" +
            "</div>";
        parrent.html(html);
    } else {
        parrent.html('<p class="text-center">Cart is empty</p>');
    }
}

$(document).ready(function() {
    $("#search input").keyup(function(){
        searchChange(this);
    });

    $('#cart > button').on('click', function (event) {
        $(this).parent().toggleClass('open');
    });

    $('body').on('click', function (e) {
        if ($('#cart .dropdown-menu').is(e.target) == false &&
            $('#cart .dropdown-menu').has(e.target).length == 0 &&
            $('.open').has(e.target).length == 0) {
            $('#cart').removeClass('open');
        }

        if ($('#preview-search').is(e.target) == false &&
            $('#preview-search').has(e.target).length == 0 &&
            !$('#preview-search').attr('disabled')) {
            $('#preview-search').hide();
            $('#search input').val('');
        }
    });
});

function searchChange(obj) {
    var local = languageSource.split('-')[0] == language ? '' : '/' + language;
    var parrent = $('#preview-search > div');

    var searchText = $(obj).val();

    if(searchText == '') {
        $('#preview-search').hide();
        parrent.html('Result is empty');
        return;
    }

    $.ajax({
        url: local + "/search",
        method: 'post',
        data: {
            'text' : searchText,
            /*'_csrf' : _csrf*/
        },
        beforeSend: function() {

        },
        success: function(data) {
            console.log(data);
            var result = JSON.parse(data);
            var html = '';

            if(result.products.length > 0) {
                result.products.forEach(function (item, i, arr) {
                    html += "<div class=\"product-item\">\n" +
                        "            <div class=\"info-part\">\n" +
                        "                <div class=\"image\">\n" +
                        "                    <img src=\"" + item.main_thumb + "\" title=\"\" alt=\"\">\n" +
                        "                </div>\n" +
                        "                <div class=\"name\">\n" +
                        "                    <a href=\"" + local + "/product/product?id=" + item.id + "\" class=\"truncate-line\">" + item.name + "</a>\n" +
                        "                    <div class=\"total\">\n" +
                        "                        <span class=\"price\">"+ item.price + "</span> UAH.\n" +
                        "                    </div>\n" +
                        "                </div>\n" +
                        "            </div>\n" +
                        "            <div class=\"add-to-cart\">\n" +
                        "                <a type=\"button\" id=\"button-cart\" onclick=\"cartAdd(" + item.id + ", 1); return false;\" class=\"btn btn-primary btn-md\"><span class=\"glyphicon glyphicon-shopping-cart\"></span></a>\n" +
                        "            </div>\n" +
                        "        </div>";
                });

                $('#preview-search').show();
            } else {
                $('#preview-search').show();
                html = 'Result is empty';

                /*setTimeout(function(){
                    if($('#preview-search .product-item').length == 0) {
                        $('#preview-search').hide();
                    }
                }, 2500);*/
            }

            parrent.html(html);
        }
    }).done(function(data) {
        /*console.log(data);*/
    });

    //alert( "Handler for .change() called." );
}