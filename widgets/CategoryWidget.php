<?php
namespace app\widgets;

use yii\base\Widget;

class CategoryWidget extends Widget
{
    public $id, $owner_id;

    public function run($params = []) {
        return $this->render('category', [
            'id'  => $this->id, 'owner_id' => $this->owner_id,
        ]);
    }
}
