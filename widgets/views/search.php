<?php
use yii\helpers\Url;
?>

<form id="search" action="<?= Url::to(['/search/find']); ?>" method="post">
    <div class="input-group">
        <input name="text" class="form-control" placeholder="Search" autocomplete="off">
        <div class="input-group-btn">
            <button class="btn btn-default" type="submit">
                <i class="glyphicon glyphicon-search"></i>
            </button>
        </div>
    </div>
</form>
<div id="preview-search">
    <div class="product-group">
    </div>
</div>