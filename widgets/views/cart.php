<?php
use yii\helpers\Url;
?>

<div id="cart" class="btn-group btn-block">
    <button type="button" class="btn btn-inverse btn-block btn-lg dropdown-toggle">
        <i class="fa fa-shopping-cart"></i>
        <span id="cart-total"><?= $cart->cart; ?></span>
    </button>
    <ul class="dropdown-menu pull-right">
        <li>
            <?php if($cart->isEmpty()) { ?>
            <p class="text-center">Cart is empty</p>
            <?php } else { ?>
                <div class="list-group">
                <?php foreach ($cart->order->products as $product) { ?>
                    <div class="list-group-item">
                        <div class="image">
                            <img src="<?= $product->main_thumb; ?>" title="<?= $product->name; ?>" alt="<?= $product->name; ?>" />
                        </div>
                        <div class="name">
                            <span class="truncate-line"><a href="<?= Url::to(['/product/product', 'id' => $product->id ]); ?>" class="link"><?= $product->name; ?></a></span>
                        </div>
                        <div class="total hidden-xs">
                            <div class="number-group">
                                <div class="input-group">
                                    <span class="input-group-addon" onclick="cartMinus(<?= $product->id; ?>, this)">-</span>
                                    <input class="form-control" value="<?= $cart->order->countProduct($product->id); ?>" readonly>
                                    <span class="input-group-addon" onclick="cartPlus(<?= $product->id; ?>, this)">+</span>
                                </div>
                            </div>
                        </div>
                        <div class="price">
                            <?= $product->price; ?> UAH.
                        </div>
                        <div class="buttons">
                            <button type="button" class="btn btn-danger" onclick="cartRemove(<?= $product->id; ?>);" >x</button>
                        </div>
                    </div>
                <?php } ?>
                    <div class="list-group-item">
                        <a href="<?= Url::to(['/order']); ?>" class="btn btn-success btn-block"><strong><i class="fa fa-share"></i> Check Out</strong></a>
                    </div>
                </div>
            <?php } ?>
        </li>
    </ul>
</div>