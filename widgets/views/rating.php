<input name="<?= $model->formName() ?>[<?= $attribute ?>]" class="raty_input" value="<?= $rating ?>" hidden>
<span class="raty_rating" data-score="<?= $rating ?>"></span>

<script>
    $(document).ready(function() {
        $('.raty_rating').raty({
            starType : 'i',
            starOff : 'fa fa-star-o',
            starOn  : 'fa fa-star',
            click: function(score, evt) {
                $('.raty_input').val(score);
            },
        });
    });
</script>