<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Url;

?>
<li>
	<div class="btn-group language-picker">
	  <?php 	      
		list($route, $params) = Yii::$app->getUrlManager()->parseRequest(Yii::$app->getRequest());

		$params = ArrayHelper::merge($_GET, $params);
		$url = isset($params['route']) ? $params['route'] : $route;
		$html   = "";
		
		foreach($languages as $language) { 		
			$url_lang = Yii::$app->urlManager->createUrl(ArrayHelper::merge(
				$params, [ $url, 'language' => $language ]
			));
							
			$url_image = Url::to('@web/uploads/flags/'.$language.'.png');

			if ($language != $currentLang) {
				$html .= '<button type="button" class="btn btn-default"><a class="active" href="'.$url_lang.'" style="text-decoration: none;">
					 <img alt="'.$language.'" class="lang-flag" src="'.$url_image.'" title="'.$language.'" width="'.$width.'">
				</a></button>';
			} else {
				$html .= '<button type="button" class="btn btn-default active"><img alt="'.$language.'" class="lang-flag" src="'.$url_image.'" title="'.$language.'" width="'.$width.'"></button>';
			}
		}
		
		echo $html;
	 
	?>
	</div>
</li>