<?php
use yii\helpers\Url;
use app\models\Category;

$categories = Category::find()->where(['owner_id' => '-1'])->all();
?>

<?php if(isset($categories)) { ?>
    <div class="list-group">
        <?php foreach ($categories as $category) { ?>
            <?php if ($category->id == $id || $category->id == $owner_id) { ?>
                <a href="<?=Url::toRoute(['/product/category', 'id' => $category->id]); ?>" class="list-group-item active"><?=$category->title?></a>
            <?php } else { ?>
                <a href="<?=Url::toRoute(['/product/category', 'id' => $category->id]); ?>" class="list-group-item"><?php echo $category->title; ?></a>
            <?php } ?>
            <?php if ($category->children) { ?>
                <?php foreach ($category->children as $child) { ?>
                    <?php if ($child->id == $id) { ?>
                        <a href="<?=Url::toRoute(['/product/category', 'id' => $child->id]); ?>" class="list-group-item active">&nbsp;&nbsp;&nbsp;- <?=$child->title?></a>
                    <?php } else { ?>
                        <a href="<?=Url::toRoute(['/product/category', 'id' => $child->id]); ?>" class="list-group-item">&nbsp;&nbsp;&nbsp;- <?=$child->title?></a>
                    <?php } ?>
                <?php } ?>
            <?php } ?>
        <?php } ?>
    </div>
<?php } ?>