<?php
//
use yii\helpers\Html;
use yii\helpers\Url;
?>

<li class="dropdown" style="margin-right: 5px">
    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
        <i class="fa fa-user"></i>
        <?php echo Yii::$app->user->isGuest ? 'User page' : Yii::$app->user->identity->username; ?>
        <span class="caret"></span>
    </a>
    <ul class="dropdown-menu dropdown-menu-right">
        <?php if(Yii::$app->user->isGuest) { ?>
            <li><a href="<?php echo Url::toRoute(['/site/login']); ?>">Login</a></li>
            <li><a href="<?php echo Url::toRoute(['/site/signup']); ?>">Sign Up</a></li>
        <?php } else { ?>
            <li><?= Html::a('Log Out', Url::to(['/site/logout']), ['data-method' => 'POST']) ?></li>
        <?php } ?>
    </ul>
</li>