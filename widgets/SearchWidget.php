<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;
use app\models\Product;

class SearchWidget extends Widget {
    public function init() {
        parent::init();
    }

    public function run($params = []) {
        return $this->render('search');
    }
}
