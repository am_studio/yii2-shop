<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;

class CartWidget extends Widget
{
    public $model;

    public function init() {
        parent::init();
    }

    public function run($params = [])
    {
        return $this->render('cart', [
            'cart'  => $this->model
        ]);
    }
}
