<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;

class MultiLanguageWidget extends Widget {
  public $calling_controller;
  public $widget_type;
  public $width;
  
  public function init()
  {
	  parent::init();
	  
	  // Exception IF params -> languages not defined
	  if (!isset(Yii::$app->urlManager->languages)) {
	   	  throw new \yii\base\InvalidConfigException("You must define Yii::\$app->urlManager->languages array like ['it', 'en', 'fr', 'de', 'es']");
	  }
	  
	  // Widget Type
	  if(!$this->width) {
	  	  $this->width = '24';
	  }
  }
  
  public function run($params = [])
  {
      $currentLang = Yii::$app->language;
      $languages   = Yii::$app->urlManager->languages;
      
      return $this->render('language', [
		  'width'       => $this->width,
          'currentLang' => $currentLang,
          'languages'   => $languages,
	  	  'controller'  => $this->calling_controller
      ]);
  }

}
