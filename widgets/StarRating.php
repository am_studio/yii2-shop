<?php
namespace app\widgets;

use Yii;
use yii\base\Widget;

class StarRating extends Widget {
    public $rating = 0;
    public $model = null;
    public $attribute = null;

    public function init() {
        parent::init();
    }

    public function run($params = []) {
        return $this->render('rating', [
            'rating' => $this->rating,
            'model' => $this->model,
            'attribute' => $this->attribute,
        ]);
    }
}
